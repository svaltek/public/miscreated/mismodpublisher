Write-Host("Started with args: " + $($args[1]) + " " + $($args[2]) + " " + $($args[3]) + " " + $($args[4]) + "")
Function Get-Folder($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms")|Out-Null
	
    $foldername = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{ SelectedPath = $initialDirectory }
    $foldername.Description = "Select a Mod folder"
    $foldername.ShowNewFolderButton = $false
	if($foldername.ShowDialog() -eq "OK")
    {
        $folder += $foldername.SelectedPath
    }
    return $folder
}



$ContentFolder = Get-Folder($args[1])


$publisher = $args[0] + "misModPublisher\misModPublisher.exe"
$arguments = "-s " + $ContentFolder + " " + $args[2] + " " + $args[3] + " " + $args[4] + "" 
start-process -NoNewWindow $publisher $arguments