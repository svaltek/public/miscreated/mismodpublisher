@echo off
set thisPATH=%~dp0


:Config
REM -- Change this if you keep your pak files somewhere else
set workshopBuildDir=Paks
REM -- Where should we find the mod.vdf -- Defaults to the same folder
set targetFolder=%thisPATH%

REM -- Dont Edit Below this Line

:START
TITLE CreateVDF: Checking...
echo Current Path: 
echo   %thisPATH%
set targetFile=%targetFolder%mod.vdf
set baseFolder=%targetFolder%
set baseFolder=%baseFolder:\=\\%
set contentFolder=%targetFolder%%workshopBuildDir%
set contentFolder=%contentFolder:\=\\%

echo Checking VDF....
echo.
IF NOT EXIST "%thisPATH%mod.vdf" ( CALL :CREATE_VDF )

:QUIT
exit



:CREATE_VDF
TITLE CreateVDF: Creating
echo  NOT FOUND... Creating...
echo --------------------------------------------------------------------------------------
echo  Target:
echo   %targetFile%
echo  ContentFolder:
echo   %contentfolder%
echo --------------------------------------------------------------------------------------
echo.   
echo  - We are now going to create the mods VDF File..
echo.   
echo  	- Lets Give it a Name....
echo.   
echo.
echo --------------------------------------------------------------------------------------
set /p modTitle="Mod Title: "
echo.
echo.
cls
echo Provide a short Description...
echo ------------------------------
echo IMPORTANT: This Should NOT contain special characters, keep things short and simple
echo Then edit on Workshop Page OR Use the description.txt to add  more info..... 
echo.
echo --------------------------------------------------------------------------------------
set /p modInfo="Enter a Description: "
echo.
echo lets wrap things up with a simple changenote....
echo ------------------------------------------------
set /p modNote="ChangeNote: "
cls
echo Workshop Tags.
echo lower case,comma seperated,no spaces 
set /p modTags="TAGS: " 

echo "workshopitem" > %targetFile%
echo { >> %targetFile%
echo 	"appid"		"299740" >> %targetFile%
echo 	"contentfolder"		"%contentFolder%" >> %targetFile%
echo    "previewfile"		"%baseFolder%previewfile.png" >> %targetFile%
echo 	"visibility"		"0" >> %targetFile%
echo 	"title"		"%modTitle%" >> %targetFile%
echo 	"description"		"%modInfo%" >> %targetFile%
echo 	"changenote"		"%modNote%" >> %targetFile%
echo 	"tags"		"%modTags%" >> %targetFile%
echo 	"publishedfileid"		"" >> %targetFile%
echo } >> %targetFile%
GOTO :EOF