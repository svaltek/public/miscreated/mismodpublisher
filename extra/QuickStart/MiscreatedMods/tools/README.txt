**************************************************************************************************************************************************************************
-                                                                       ADDITIONAL TOOLS                                                                                 -
**************************************************************************************************************************************************************************

createVDF.bat
-------------

	creates a vdf file using relative paths place in your mods Root folder
	assumes you put your pak files in ModRootFolder/paks
	and have a png previewfile ModRootFolder/previewfile.png

   EXTRA
-----------

The Following files Need Powershell Scripts Enabled but allow you to Select your Mod folder using a FolderBrowser Dialog
You can find them in the tools Directory, but they need to be moved to this directory befor use...


PublishMod.bat
	Publish mod Directly using values from mod.vdf

PublishMod-descriptionfile.bat
	Publish Mod Using Description from ModRootFolder/description.txt



_MisModPublisherTest.bat
	Publish the TestMod to Check functionality
	(need to run createVDF in the Mods/TestMod folder first)