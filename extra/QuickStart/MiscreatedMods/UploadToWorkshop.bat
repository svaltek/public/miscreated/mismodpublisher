@echo off
set thisPATH=%~dp0

:Config
REM -- Change this to path to misModPublisher - shift rightclick on the misModPublisher.exe and copy as path then paste it here
set misModPublisherPath=..\..\misModPublisher\misModPublisher.exe
echo Current Path: %thisPATH%
IF NOT EXIST "%misModPublisherPath%" ( CALL :misModPublisherMissing )

REM -- to use the descriptionfile.txt ad the -d switch to the ennd of the next line
%misModPublisherPath% -s %thisPATH% 

ECHO Remember to copy you Mod id from publishfileId.txt to your VDF


exit
:misModPublisherMissing
echo Couldnt find misModPublisher.exe at path:%misModPublisherPath%