

**************************************************************************************************************************************************************************

                                                                    --- MisModPublisher --- 

                                                    An Alternative Tool To Upload Your Miscreated Mods On Steam

**************************************************************************************************************************************************************************



**************************************************************************************************************************************************************************
-                                                                    WHAT IS THIS ABOUT ?                                                                                -
**************************************************************************************************************************************************************************

MisModPublisher is a tool designed to simplify the process of uploading and updating your mods on Steam.
Easier and more convenient to use than the regular SteamCMD, this application lets you :

- Create a reference file (vdf) for your mod, including : Title, Description, Changenotes, Steam Tags ...
- Upload/update your mod on Steam without disconnecting your Steam account.
- Automatically open your mod page at the end of the upload to let you personnalize it.

We are going to detail the process of each steps in the subsections below.



**************************************************************************************************************************************************************************
-                                                                        INSTALLATION                                                                                    -
**************************************************************************************************************************************************************************

1 - Download misModPublisher at this website : http://svaltek.xyz/projects/mismodpublisher/
2 - Choose method 1 : Quickstart, and download the zip file.
3 - Extract the content of the zip file.



**************************************************************************************************************************************************************************
-                                                                          HOW TO USE                                                                                    -
**************************************************************************************************************************************************************************

#1. Copy your Mod freshly created into the MiscreatedMods/Mods folder.

#2. Open your mod.vdf file
   >> NOTES : if you don't have any reference file (.vdf) you can simply create one using tools/createVDF.bat (instructions below) and jump to #6.

#3. Edit the paths of "contentfolder" and "previewfile" to match the new locations. 
   >> NOTES : for example, if you have extracted misModPublisher in your desktop, that would be:
    "C:\\Users\\[YOURNAME]\\Desktop\\MiscreatedMods\\Mods\\[YOURMOD]\\Paks" and "C:\\Users\\[YOURNAME]\\Desktop\\MiscreatedMods\\Mods\\[YOURMOD]\\Images\\[YOURIMAGE]"

#4. Be sure to fill every following queries : Title / Description / Changenote --

#5. You can also add Steam Tags to better detail your mod. For example : "tags"   "admin,item,weapons"
   >> NOTES : - Only use lowercase characters. THIS IS IMPORTANT.

#6. Go back to misModPublisher main folder and copy/past the UploadToWorkshop.bat file into your Mod folder >> where your mod.vdf is.

#7. Double click on your UploadToWorkshop.bat to start the upload. misModPublisher will automatically open your brand new mod page.


IMPORTANT : a new txt file has been created into your mod folder, containing your mod ID. 
Copy that ID into your vdf file, in the "publishedfileid" query.


you can also use the changelog.txt and description.txt to set your mods description/changelog
to enable these you need to edit the UploadToWorkshop bat and change the following line

%misModPublisherPath% -s %thisPATH% 

to enable description.txt add -d before the -s switch
to enable changelog.txt add -n befor the -s switch

so to enable both the line would look like below
%misModPublisherPath% -n -d -s %thisPATH% 



**************************************************************************************************************************************************************************
-                                                                       ADDITIONAL TOOLS                                                                                 -
**************************************************************************************************************************************************************************

--- createVDF ---

To be found in: MiscreatedMods/Tools

If you want to be guided during the creation of your vdf, you can use that very convenient tool. 
Place it in your mod folder, launch it and let it guide you. It will create a new vdf file ready to use.


**************************************************************************************************************************************************************************
-                                                                           CREDITS                                                                                      -
**************************************************************************************************************************************************************************

    -> PitiViers, for Writing the ReadMe and for their Help With Testing & Debugging.

**************************************************************************************************************************************************************************
-                                                                        USEFUL LINKS                                                                                    -
**************************************************************************************************************************************************************************

Infos and Contact : http://svaltek.xyz

Miscreated Official Discord : https://discord.gg/X8HJM6S

Miscreated Modding Discord (Unofficial) : https://discord.gg/BbEhbTA

Miscreated Workshop : https://steamcommunity.com/app/299740/workshop/


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
That's all folks ! :)