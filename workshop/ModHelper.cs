/**
misModPublisher
    Copyright (C) 2020  Svaltek / Public

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
// ModHelper.cs
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Gameloop.Vdf;
using Steamworks;
namespace misModPublisher
{
    public class Mod
    {
        public string VDFpath { get; set; }
        private PublishedFileId_t _publishedFileId = PublishedFileId_t.Invalid;
        public PublishedFileId_t PublishedFileId
        {
            get => _publishedFileId;
            set
            {
                if (_publishedFileId != value && value != PublishedFileId_t.Invalid)
                    File.WriteAllText(PathCombine(VDFpath, "PublishedFileId.txt"), value.ToString());
                _publishedFileId = value;
            }
        }
        public string ContentFolder { get; }
        public string Name { get; }
        public string Preview{ get; }
        public string Description { get; set; }
        public bool OriginalUploader { get; set; }
        public dynamic Visibility { get; private set; }
        public dynamic ChangeNote { get; set; }

        public List<string> Tags;

        public Mod ( string path, bool originalUploader = true )
        {
            if ( !Directory.Exists( path ) )
            {
                throw new Exception( $"path '{path}' not found." );
            }
            OriginalUploader = originalUploader;
            Tags = new List<string>();

            // open mod.vdf
            VDFpath = path;
            var ModVDF = PathCombine(VDFpath,"mod.vdf");
            if ( !File.Exists( ModVDF ) )
            {
                throw new Exception( $"file not found ({ModVDF})");
            }
            dynamic ModInfo = VdfConvert.Deserialize(File.ReadAllText(ModVDF));


            // get mod title
            if ((ModInfo.Value.title).ToString() != "")
            {
                Name = (ModInfo.Value.title).ToString();
            }
            else
            {
                throw new Exception("couldnt read key: title from vdf");
            }
            // get mod description
            if ((ModInfo.Value.description).ToString() != "")
            {
                Description = (ModInfo.Value.description).ToString();
            }

            // get mod visibility
            if ((ModInfo.Value.visibility).ToString() != "")
            {
                Visibility = (ModInfo.Value.visibility).ToString();
            }

            // get content folder
            if ((ModInfo.Value.contentfolder).ToString() != "")
            {
                ContentFolder = (ModInfo.Value.contentfolder).ToString();
            }
            else
            {
                throw new Exception("couldnt read key: contentfolder from vdf");
            }

            // get preview image
            if ((ModInfo.Value.previewfile).ToString() != "")
            {
                var preview = (ModInfo.Value.previewfile).ToString();
                if (File.Exists(preview))
                    Preview = preview;
            }

            // get mod changenote
            if ((ModInfo.Value.changenote).ToString() != "")
            {
                ChangeNote = (ModInfo.Value.changenote).ToString();
            }

            // get publishedFileId
            if ((ModInfo.Value.publishedfileid).ToString() != "" )
            {
                string idStr = (ModInfo.Value.publishedfileid).ToString();
                uint id = Convert.ToUInt32(idStr);
                PublishedFileId = new PublishedFileId_t( id );
            }
            else
            {
                PublishedFileId = PublishedFileId_t.Invalid;
            }

            // get tags
            if ((ModInfo.Value.tags).ToString() != "")
            {
                string[] tagsArray = (ModInfo.Value.tags).ToString().Split(',');
                Tags = tagsArray.ToList();
            }
        }

        public void TimeStamp()
        {
            File.WriteAllText( PathCombine( ContentFolder, "timestamp.txt" ), DateTime.Now.ToString() );
        }

        private static string PathCombine(params string[] parts)
        {
            return string.Join(Path.DirectorySeparatorChar.ToString(), parts);
        }
    }
}
